# Personal dictionary and word memorizing app.

### Steps to follow for Linux
- git clone https://bitbucket.org/kerimlcr/memapp.git
- cd memapp/
- python3 -m venv .venv
- source .venv/bin/activate
- pip install -r req.txt
- flask run

### Steps to follow for Windows
- git clone https://bitbucket.org/kerimlcr/memapp.git
- cd memapp/
- py -m venv env
- env\Scripts\activate
- pip install -r req.txt
- flask run