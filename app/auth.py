from flask import Blueprint, request, redirect, url_for, flash, render_template
from flask_login import login_user, logout_user, login_required, current_user
from werkzeug.security import generate_password_hash, check_password_hash
from sqlalchemy import and_
from .models import User, Words, Relations
from . import db
import requests
import json
import random

auth = Blueprint('auth', __name__)


@auth.route('/login', methods=["POST"])
def login():
    email = request.form.get('email')
    password = request.form.get('password')
    user = User.query.filter_by(email=email).first()
    if not user or not check_password_hash(user.password, password):
        flash('Please check your login details and try again.')
        return redirect(url_for('auth.login'))
    login_user(user)
    return redirect(url_for("main.index"))


@auth.route('/register', methods=["POST"])
def register():
    username = request.form.get('username')
    email = request.form.get('email')
    password = request.form.get('password')

    user = User.query.filter_by(email=email).first()
    if user:
        flash('Email address already exists')
        return redirect(url_for("auth.register"))
    new_user = User(username=username, password=generate_password_hash(password, method='sha256'), email=email)

    db.session.add(new_user)
    db.session.commit()

    return redirect(url_for('auth.login'))


@auth.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('main.index'))


@auth.route('/search', methods=["POST"])
@login_required
def search():
    user_id = current_user.id
    keyword = request.form.get('sbox')
    exits = Words.query.filter_by(keyword=keyword).first()
    # If the search word is registered
    if exits:
        relations_keyword = Relations.query.filter_by(uid=user_id, wid=exits.id).first()
        # If not the word is associated with the user
        if not relations_keyword:
            insert_data = Relations(uid=user_id, wid=exits.id)
            db.session.add(insert_data)
            db.session.commit()
            flash("The word you are looking for has been added to your personal dictionary.")
            return render_template('index.html', name=current_user.username,
                                   data={"keyword": keyword, "definition": json.loads(exits.definition)})
        # Update search count
        relations_keyword.search_count += 1
        db.session.commit()
        return render_template('index.html', name=current_user.username,
                               data={"keyword": keyword, "definition": json.loads(exits.definition)})

    url = "https://wordsapiv1.p.rapidapi.com/words/{}".format(keyword)

    headers = {
        "x-rapidapi-key": "e2ac147cc5mshfc719ff57c8c517p19fea9jsnb76c21c8b94a",
        "x-rapidapi-host": "wordsapiv1.p.rapidapi.com"
    }

    response = requests.request("GET", url, headers=headers)
    response = response.json()
    try:
        definition = get_all_definition(response["results"])
        insert_data = Words(
            keyword=response["word"],
            definition=definition,
            weight=response["frequency"]
        )

        db.session.add(insert_data)
        db.session.commit()
        # Relations create
        word_id = insert_data.id
        insert_relations_data = Relations(uid=user_id, wid=word_id)
        db.session.add(insert_relations_data)
        db.session.commit()
        flash("The word you are looking for has been added to your personal dictionary.")
        return render_template('index.html',
                               name=current_user.username,
                               data={"keyword": keyword, "definition": json.loads(definition)})
    except KeyError:
        return render_template('index.html',
                               name=current_user.username,
                               data={"msg": "No such word found"})


@auth.route('/my-words', methods=["GET"])
@login_required
def my_words():
    keywords = {}
    point = {}
    word_ids = Relations.query.filter_by(uid=current_user.id).all()

    for index in range(len(word_ids)):
        word = Words.query.filter_by(id=word_ids[index].wid).first()
        keywords[word.keyword] = json.loads(word.definition)
        point[word.keyword] = word_ids[index].point

    return render_template('my-word.html', data=keywords, points=point)


@auth.route('/exercise', methods=["GET", "POST"])
@login_required
def exercise():
    questions = {}
    other_words = []
    words = Relations.query.filter_by(uid=current_user.id).join(Words, Relations.wid == Words.id)\
        .add_columns(Words.keyword, Words.definition).all()
    # User-owned word count control
    if len(words) < 10:
        flash('If you have enough word, all you have to do is search for a word.')
        return render_template('exercise.html')

    # It continues until there is only one word
    # Meanwhile collects random answers
    while len(words) != 1:
        try:
            remove_index = random.randint(0, len(words))
            other_word = words[remove_index][1]
            other_words.append(other_word)
            del words[remove_index]
        except IndexError:
            continue
    # Loop for more than one word, but we use one word
    # Creates the answers
    for index in range(0, len(words)):
        # For different random number
        rand1 = random.randint(0, len(other_words) - 1)
        rand2 = random.randint(0, len(other_words) - 1)
        while rand1 == rand2:
            rand2 = random.randint(0, len(other_words) - 1)

        definition = json.loads(words[index][2])
        ans = [
            words[0][1],
            other_words[rand1],
            other_words[rand2]
        ]
        # Shuffle the answers
        random.shuffle(ans)

        questions[index] = {
            "definition": "{}".format(definition["{}".format(random.randint(0, len(definition) - 1))]),
            "keywords": ans}

    # If the user did not answer the question
    if request.method == "POST":
        return {"data": questions}
    return render_template('exercise.html', data=questions)


@auth.route('/check-answer', methods=["POST"])
@login_required
def answer_question():
    options = json.loads(request.form.get("options"))  # all words
    answer = request.form.get("answer")  # user answer
    definition = request.form.get("definition")  # word definition
    # If the choice is correct update point and return True
    query = Words.query.filter_by(keyword=answer).filter(Words.definition.like("%{}%".format(definition))).first()

    if query:
        q = Relations.query.join(Words, Relations.wid == Words.id).add_columns(Words.keyword, Relations.point).\
            filter(and_(Words.keyword.like("{}".format(answer)), Relations.uid == current_user.id)).first()
        if q[1] == answer:
            update_query = Relations.query.filter(and_(Relations.id == q[0].id, Relations.uid == current_user.id)).first()
            update_query.point = calculate_power(q[0].wid, 1)
            db.session.commit()
            return {"msg": True}
        
    # Decreases scores on words with incorrect selection
    for item in options:
        q = Relations.query.join(Words, Relations.wid == Words.id).\
            filter(and_(Words.keyword.like("{}".format(item)), Relations.uid == current_user.id)).add_columns(Relations.point).first()
        update_query = Relations.query.filter_by(id=q[0].id).first()
        update_query.point = calculate_power(q[0].wid, -2)
        db.session.commit()
    return {"msg": False}


def calculate_power(word_id, point):
    """
    This function calculate user question points
    :param word_id: int
    :param point: int
    :return: int
    """
    result = Relations.query.filter_by(uid=current_user.id, wid=word_id).first()
    return int(point + (result.point * result.search_count))


def get_all_definition(data):
    """
    This functions collects definitions in the returned response
    :param data: json, dict
    :return: str
    """
    r_dict = {}
    # If there is one definition, in order not to get an error
    # Example: [{
    # 'definition': 'an implement used to erase something',
    # 'partOfSpeech': 'noun', 'typeOf': ['implement']
    # }]
    if len(data) == 1:
        r_dict[0] = data[0]["definition"]
        return json.dumps(r_dict)
    for index in range(0, len(data) - 1):
        r_dict[index] = data[index]["definition"]
    return json.dumps(r_dict)
