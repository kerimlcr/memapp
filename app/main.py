from flask import Blueprint, render_template, redirect, url_for
from flask_login import login_required, current_user
app = Blueprint('main', __name__)


@app.route("/")
@login_required
def index():
    return render_template("index.html", name=current_user.username)


@app.route("/login")
def login():
    if current_user.is_authenticated:
        return redirect(url_for("main.index"))
    return render_template("login.html")


@app.route("/register")
def register():
    if current_user.is_authenticated:
        return redirect(url_for("main.index"))
    return render_template("register.html")