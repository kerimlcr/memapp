from . import db
from flask_login import UserMixin


class User(db.Model, UserMixin):
    id = db.Column("id", db.Integer, primary_key=True)
    username = db.Column("username", db.String(100))
    password = db.Column("password", db.String(100))
    email = db.Column("email", db.String(100), unique=True)

    def __repr__(self):
        return f"User('{self.username}', '{self.email}')"


class Relations(db.Model):
    id = db.Column("id", db.Integer, primary_key=True)
    uid = db.Column("uid", db.Integer, db.ForeignKey('user.id'))
    wid = db.Column("wid", db.Integer, db.ForeignKey('words.id'))
    search_count = db.Column("search_count", db.Integer, default=1)
    point = db.Column("point", db.Integer, default=0)


class Words(db.Model):
    id = db.Column("id", db.Integer, primary_key=True)
    keyword = db.Column("keyword", db.String(100))
    definition = db.Column("definition", db.String(1000))
    weight = db.Column("weight", db.Integer)
